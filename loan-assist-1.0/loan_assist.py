# Copyright 2015 Joshua J.D. Matzek

# This program retrieves loan data for a Navient account and calculates
# accrued interest for each loan.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bs4 import BeautifulSoup
import requests
import sys
import getpass

verbose = False # Do not print out detailed session info by default. 

# First stage of authentication
payload_login_page_url_encoded = {
	'command':'login',
	'returnUrl':'https%3A%2F%2Fwww.manageyourloans.com%2FMYL%3FStateName%3Dglobal_CALMLandingPage',
	'destAppName':'NAVCOM',
	'clientTimeZoneAndScreenSize':'US%2FEastern9001600',
	'UserID':'',
	'Password':''
}
	
# Second stage of authentication.
payload_verification_page_url_encoded = {
	'command':'validateSSNOrChallengeAnswer',
	'returnUrl':'https%3A%2F%2Fwww.manageyourloans.com%2FMYL%3FStateName%3Dglobal_CALMLandingPage',
	'destAppName':'NAVCOM',
	'SSN1':'',
	'SSN2':'',
	'SSN3':'',
	'dSSN1':'',
	'dSSN2':'',
	'dSSN3':'',
	'ssnOrAcc':'rbAcc',
	'accountNumber':'',
	'accountNumber1':'',
	'taccountNumber':'**********',
	'month':'',
	'day':'',
	'year':'',
	'month1':'',
	'day1':'',
	'year1':'',
	'tmonth':'**',
	'tday':'**',
	'tyear':'****',
	'Submit':'Submit'
}

def main():
	user = input('Please enter your username: ')
	password = getpass.getpass('Please enter your password: ')
	account_number = input('Please enter your account number: ')
	birthday = input('Please enter your birthday (YYYY-MM-DD): ')

	response = login(user, password, account_number, birthday)

	if (response.status_code == 503): 
		print('Service is temporarily unavailable.  Please try again later.')
	else:
		display_table(response.text)
	


def login(user, password, account_number, birthday):

	login_frame = 'https://login.navient.com/CALM/calm.do?sourceAppName=NAVCOM'
	login_action = 'https://login.navient.com/CALM/login.do'
	convergence_page_1 = 'https://www.manageyourloans.com/MYL2/convergence?execution=e1s1&_eventId=getTestCookie'
	convergence_page_2 = 'https://www.manageyourloans.com/MYL2/convergence?execution=e1s2&_eventId=finalizeConvergence'
	
	birthday_strings = birthday.split('-')
	
	payload_login_page_url_encoded['UserID'] = user
	payload_login_page_url_encoded['Password'] = password
	payload_verification_page_url_encoded['accountNumber'] = account_number
	payload_verification_page_url_encoded['accountNumber1'] = account_number
	if (len(birthday_strings) == 3):
		payload_verification_page_url_encoded['day'] = birthday_strings[2]
		payload_verification_page_url_encoded['month'] = birthday_strings[1]
		payload_verification_page_url_encoded['year'] = birthday_strings[0]
		payload_verification_page_url_encoded['day1'] = birthday_strings[2]
		payload_verification_page_url_encoded['month1'] = birthday_strings[1]
		payload_verification_page_url_encoded['year1'] = birthday_strings[0]
	
	session = requests.Session()

	print('GETting ' + login_frame + ' ...')
	response = session.get(login_frame)
	if (verbose): print_verbose_messages(session, response)
	
	print('POSTing user credentials to ' + login_action + ' ...')
	response = session.post(login_action, data=payload_login_page_url_encoded)
	if (verbose): print_verbose_messages(session, response)

	print('POSTing account number to ' + login_action + ' ...')
	response = session.post(login_action, data=payload_verification_page_url_encoded)
	if (verbose): print_verbose_messages(session, response)

	print('GETting ' + convergence_page_1 + ' ...')
	response = session.get(convergence_page_1)
	if (verbose): print_verbose_messages(session, response)
	
	print('GETting ' + convergence_page_2 + ' ...')
	response = session.get(convergence_page_2)
	if (verbose): print_verbose_messages(session, response)

	return response
	
def display_table(html):
	soup = BeautifulSoup(html)
	clean_headers = []
	headers = soup.find('table', class_='billing-group').find_all('th')
	loan_name = soup.find_all('td', class_='loan-name')
	loan_balance = soup.find_all('td', class_='loan-balance')
	int_rate = soup.find_all('td', class_='int-rate')
	min_pay_due = soup.find_all('td', class_='min-pay-due')
	pay_due_date = soup.find_all('td', class_='pay-due-date')

	#Remove unwanted blocks of html BeautifulSoup found
	min_pay_due.pop(0)
	min_pay_due.pop()
	total = loan_balance.pop(0)
	loan_balance.pop()

	loan_name_string = ''
	
	#Get the innerHtml of the header tags.
	for h in headers:
		clean_headers.append(" ".join(h.find_all(text=True)))
	
	#For formatting the accrued interest calculated.
	format_currency = lambda value, currency: "%s%s%s" %(
		"-" if value < 0 else "", 
		currency, 
		('{:%d,.2f}'%(len(str(value))+3)).format(abs(value)).lstrip())

	print('-------------------------------------------------------------------------------')
	print('---------------------------------Loan Summary----------------------------------\n')
	print('Grand Total: ' + "".join(total.string.split()) + '\n')
	# Print the headers in 3 separate rows so they 
	# fit within the width of the console.
	print(clean_headers[0].ljust(25), 
	clean_headers[1].split(' ')[0].ljust(12), 
	clean_headers[2].split(' ')[0].ljust(8), 
	'Accrued'.ljust(8), 
	clean_headers[3].split(' ')[0].ljust(8), 
	clean_headers[4].split(' ')[0].ljust(12))
	print(''.ljust(25), 
	clean_headers[1].split(' ')[1].ljust(12), 
	clean_headers[2].split(' ')[1].ljust(8), 
	'Interest'.ljust(8), 
	clean_headers[3].split(' ')[1].ljust(8), 
	(clean_headers[4].split(' ')[1] + ' ' + clean_headers[4].split(' ')[2]).ljust(12))
	print(''.ljust(25), 
	''.ljust(12), 
	clean_headers[2].split(' ')[2].ljust(8), 
	''.ljust(8), 
	''.ljust(8), 
	''.ljust(12))
	print()
	# Print all loan data.
	for name, balance, rate, min_pay, due_date in zip(loan_name, loan_balance, int_rate, min_pay_due, pay_due_date):
		for lns in name.stripped_strings:
			loan_name_string = loan_name_string + ' ' + lns
		for b,r,m,d in zip(balance.stripped_strings, rate.stripped_strings, min_pay.stripped_strings, due_date.stripped_strings):
			balance_stripped_formatting = float(b.replace('$', '').replace(',',''))
			interest_stripped_formatting = float(r.replace('%',''))/100
			interest = balance_stripped_formatting * interest_stripped_formatting
			interest_formatted = format_currency(interest, "$")
			print(loan_name_string.lstrip().ljust(25), b.ljust(12), r.ljust(8), interest_formatted.ljust(8), m.ljust(8), d.ljust(12))
		loan_name_string = ''
	print('-------------------------------------------------------------------------------')
	print('-------------------------------------------------------------------------------')
	

	
def print_verbose_messages(session, response):
	print('\nStatus Code: ')
	print(str(response.status_code) + '\n')
	print('Cookies: ')
	for key, value in session.cookies.items():
		print(key + '..........' + value)
	print()
	print('History: ') # Show if there were any 302 redirects that Requests session handled.
	print(str(response.history) + '\n')

if __name__ == '__main__':
	# Check if user wants verbose messaging.
	for arg in sys.argv:
		if (arg == '-v'):
			verbose = True
	if (verbose == False): 
		print('loan-assist-1.0 is running.  Verbosity is off by default.  Run with command line flag \'-v\' to include detailed session information for each http request.')
	else:
		print('Verbose messages are on.')
	main()
	
